import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

public class testGeneral
{
    Armee armee, armee2;
    General general;
    List<Soldat> soldats;
    Soldat firstSoldat, secondeSoldat, thirdSoldat;

    @BeforeEach
    void init()
    {
        general = new General("Jack","grade",100, 70,100,5,3);

        armee = new Armee("9eme Legion", null);
        armee2 = new Armee("8eme Legion", null);

        firstSoldat = new Soldat("Bob", "grade", 30,30,30,1);
        secondeSoldat = new Soldat("Michel", "grade", 30,30,30,1);
        thirdSoldat = new Soldat("Henry", "grade", 30,30,30,1);

        soldats = new ArrayList<>() ;
        soldats.add(firstSoldat);
        soldats.add(secondeSoldat);
        soldats.add(thirdSoldat);
    }

    @Test
    void testChangeArmee()
    {
        general.changeArmee(armee);
        assertEquals(general.getArmee(), armee,"Le général n'a pas reçu l'armée");
        assertEquals(general, armee.getGeneral(),"L'armée n'a pas été affecté au général");

        general.changeArmee(armee2);
        assertEquals(general.getArmee(), armee2,"Le général n'a pas reçu l'armée 2");
        assertEquals(general, armee2.getGeneral(),"L'armée 2 n'a pas été affecté au général");
    }


}
