import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.not;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class testArmee
{
    Armee armee;
    General general, general2;
    List<Soldat> soldats;
    Soldat firstSoldat, secondeSoldat, thirdSoldat;

    @BeforeEach
    void init() {
        general = new General("Jack", "grade", 100, 70, 100, 5, 3);
        general2 = new General("Peter", "grade", 100, 70, 100, 5, 3);

        armee = new Armee("9eme Legion", null);

        firstSoldat = new Soldat("Bob", "grade", 30, 30, 30, 1);
        secondeSoldat = new Soldat("Michel", "grade", 30, 30, 30, 1);
        thirdSoldat = new Soldat("Henry", "grade", 30, 30, 30, 1);

        soldats = new ArrayList<>();
        soldats.add(firstSoldat);
        soldats.add(secondeSoldat);
        soldats.add(thirdSoldat);
    }

    @Test
    void testChangeGeneral()
    {
        armee.changeGeneral(general);
        assertEquals(general.getArmee(), armee, "L'armée n'a pas été affecté au général");
        assertEquals(armee.getGeneral(), general, "L'armée n'a pas reçu le général");

        armee.changeGeneral(general2);
        assertEquals(general2.getArmee(), armee, "L'armée n'a pas été affecté au général 2");
        assertEquals(armee.getGeneral(), general2, "L'armée n'a pas reçu le général 2");
    }

    @Test
    void testAddSoldat()
    {
        assertTrue(armee.getSoldats().isEmpty(), "L'armée n'est pas vide");

        armee.addSoldat(firstSoldat);
        armee.addSoldat(secondeSoldat);

        assertThat("L'armée n'a pas reçu le premier soldat", armee.getSoldats(), hasItem(firstSoldat));
        assertThat("L'armée n'a pas reçu le deuxième soldat", armee.getSoldats(), hasItem(secondeSoldat));
        assertThat("L'armée contient un soldat en trop", armee.getSoldats(), not(hasItem(thirdSoldat)));
    }
}
