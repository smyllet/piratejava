import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.not;

public class testNavire
{
    Marin marin1, marin2, capitaine;
    List<Marin> equipage;
    Pirate barbeNoir;
    Navire navire;

    @BeforeEach
    void init()
    {
        marin1 = new Marin("Robert","mousse");
        marin2 = new Marin("Dupond", "Lieutenant");
        capitaine = new Capitaine("Ackab", "Capitaine","Senior");

        barbeNoir = new Pirate("Barbe Noir", "Traitre", 35);

        equipage = new ArrayList<>();
        equipage.add(marin1);
        equipage.add(marin2);
        equipage.add(capitaine);

        navire = new Navire(60,"corvette", equipage);
    }

    @Test
    void testAjoutMarin()
    {
        Marin moussaillon = new Marin("Roger", "Moussaillons");
        navire.addMarin(moussaillon);
        assertThat("Échec d'ajout d'un marin", equipage, hasItem(moussaillon));

        Marin junior = new Marin("Ulysse", "Moussaillons");
        assertThat("Un marin a été ajouté par magie", equipage, not(hasItem(junior)));
    }

    @Test
    void testRetraitMarin()
    {
        navire.removeMarin(marin1);
        assertThat("Le marin n'a pas été retiré correctement de l'équipage", equipage, not(hasItem(marin1)));
    }
}
