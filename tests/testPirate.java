import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.not;
import static org.junit.jupiter.api.Assertions.*;

public class testPirate
{
    Marin marin1, marin2, capitaine;
    List<Marin> equipage;
    Pirate barbeNoir;
    Navire navire;

    @BeforeEach
    void init()
    {
        marin1 = new Marin("Robert","mousse");
        marin2 = new Marin("Dupond", "Lieutenant");
        capitaine = new Capitaine("Ackab", "Capitaine","Senior");

        barbeNoir = new Pirate("Barbe Noir", "Traitre", 35);

        equipage = new ArrayList<>();
        equipage.add(marin1);
        equipage.add(marin2);
        equipage.add(capitaine);

        navire = new Navire(60,"corvette", equipage);
    }

    @Test
    void testEgalitePirate()
    {
        Pirate pirateDeRemplacement = new Pirate("Pierre", "Traitre", 35);
        assertTrue(barbeNoir.equals(pirateDeRemplacement));
        Pirate encoreUnAutrePirate = new Pirate("Théo", "Traitre", 2);
        assertFalse(barbeNoir.equals(encoreUnAutrePirate));
    }

    @Test
    void testPiratePeuSauvage()
    {
        barbeNoir.pillage(navire);
        assertThat("Le capitaine est toujours en vie", navire.getEquipage(), not(hasItem(capitaine)));
        assertNotEquals(0 ,navire.getEquipage().size(), "Le pirate a été plus sauvage que prévu");
    }

    @Test
    void testPirateTresSauvage()
    {
        barbeNoir.setDegreSauvagerie(70);
        barbeNoir.pillage(navire);
        assertEquals(0 ,navire.getEquipage().size(), "L'équipage à résisté");
    }
}
