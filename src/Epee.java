public class Epee extends Arme implements CorpsACorps
{
    private int taille;
    private boolean uneMain;

    public Epee(int durabilite, int degats, int defense, int taille, boolean uneMain) {
        super(durabilite, degats, defense);
        this.taille = taille;
        this.uneMain = uneMain;
    }

    public int getTaille() {
        return taille;
    }

    public void setTaille(int taille) {
        this.taille = taille;
    }

    public boolean isUneMain() {
        return uneMain;
    }

    public void setUneMain(boolean uneMain) {
        this.uneMain = uneMain;
    }

    @Override
    public String toString() {
        return "Epee{" +
                "taille=" + taille +
                ", uneMain=" + uneMain +
                '}';
    }

    @Override
    public void combatRapproche() {
        // todo
    }
}
