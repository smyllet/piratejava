import java.util.ArrayList;
import java.util.Objects;

public class Pirate extends Marin implements Piraterie
{
    private int degreSauvagerie;
    private static final int SEUIL_SAUVAGERIE = 50;

    public Pirate(String nom, String fonction, int degreSauvagerie)
    {
        super(nom, fonction);
        this.degreSauvagerie = degreSauvagerie;
    }

    public int getDegreSauvagerie()
    {
        return degreSauvagerie;
    }

    public void setDegreSauvagerie(int degreSauvagerie)
    {
        this.degreSauvagerie = degreSauvagerie;
    }

    @Override
    public void pillage(Navire navire)
    {
        if(this.getDegreSauvagerie() > SEUIL_SAUVAGERIE)
            navire.setEquipage(new ArrayList<>());
        else
            navire.getEquipage().removeIf(marin -> marin instanceof Capitaine);
    }

    public boolean equals(Pirate pirate)
    {
        return this.getFonction().equals(pirate.getFonction()) && (this.getDegreSauvagerie() == pirate.getDegreSauvagerie());
    }

    @Override
    public int hashCode() {
        return Objects.hash(degreSauvagerie);
    }

    @Override
    public String toString()
    {
        return "Pirate{" +
                "degreSauvagerie=" + degreSauvagerie +
                '}';
    }
}