public class Soldat
{
    private String nom, grade;
    private int sante, force, endurance, competence;

    public Soldat(String nom, String grade, int sante, int force, int endurance, int competence) {
        this.nom = nom;
        this.grade = grade;
        this.sante = sante;
        this.force = force;
        this.endurance = endurance;
        this.competence = competence;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

    public int getSante() {
        return sante;
    }

    public void setSante(int sante) {
        this.sante = sante;
    }

    public int getForce() {
        return force;
    }

    public void setForce(int force) {
        this.force = force;
    }

    public int getEndurance() {
        return endurance;
    }

    public void setEndurance(int endurance) {
        this.endurance = endurance;
    }

    public int getCompetence() {
        return competence;
    }

    public void setCompetence(int competence) {
        this.competence = competence;
    }

    @Override
    public String toString() {
        return "Soldat{" +
                "nom='" + nom + '\'' +
                ", grade='" + grade + '\'' +
                ", sante=" + sante +
                ", force=" + force +
                ", endurance=" + endurance +
                ", competence=" + competence +
                '}';
    }
}
