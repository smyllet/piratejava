import java.util.ArrayList;
import java.util.List;

public class Armee
{
    private String nom;
    private General general;
    private List<Soldat> soldats;

    public Armee(String nom, General general) {
        this.nom = nom;
        this.setGeneral(general);
        this.soldats = new ArrayList<>();
    }

    public Armee(String nom, General general, List<Soldat> soldats) {
        this.nom = nom;
        this.setGeneral(general);
        this.soldats = soldats;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public List<Soldat> getSoldats() {
        return soldats;
    }

    public void setSoldats(List<Soldat> soldats) {
        this.soldats = soldats;
    }

    public General getGeneral() {
        return general;
    }

    public void setGeneral(General general) {
        this.general = general;
    }

    public void changeGeneral(General general)
    {
        if(general != null) if (general.getArmee() != null) general.getArmee().setGeneral(null);
        this.general = general;
        general.setArmee(this);
    }

    public void addSoldat(Soldat soldat)
    {
        if((soldat != null) && (!soldats.contains(soldat))) soldats.add(soldat);
    }

    @Override
    public String toString() {
        return "Armee{" +
                "nom='" + nom + '\'' +
                ", soldats=" + soldats +
                ", general=" + general +
                '}';
    }
}
