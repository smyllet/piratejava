public class Arc extends Arme implements Distance
{
    private int taille;
    private int portee;

    public Arc(int durabilite, int degats, int defense, int taille, int portee) {
        super(durabilite, degats, defense);
        this.taille = taille;
        this.portee = portee;
    }

    public int getTaille() {
        return taille;
    }

    public void setTaille(int taille) {
        this.taille = taille;
    }

    public int getPortee() {
        return portee;
    }

    public void setPortee(int portee) {
        this.portee = portee;
    }

    @Override
    public String toString() {
        return "Arc{" +
                "taille=" + taille +
                ", portee=" + portee +
                '}';
    }

    @Override
    public void aDistance() {
        // todo
    }
}
