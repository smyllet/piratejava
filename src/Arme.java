public class Arme
{
    private int durabilite, degats, defense;

    public Arme(int durabilite, int degats, int defense) {
        this.durabilite = durabilite;
        this.degats = degats;
        this.defense = defense;
    }

    public int getDurabilite() {
        return durabilite;
    }

    public void setDurabilite(int durabilite) {
        this.durabilite = durabilite;
    }

    public int getDegats() {
        return degats;
    }

    public void setDegats(int degats) {
        this.degats = degats;
    }

    public int getDefense() {
        return defense;
    }

    public void setDefense(int defense) {
        this.defense = defense;
    }

    @Override
    public String toString() {
        return "Arme{" +
                "durabilite=" + durabilite +
                ", degats=" + degats +
                ", defense=" + defense +
                '}';
    }
}
