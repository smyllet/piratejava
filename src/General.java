public class General extends Soldat
{
    int nbEtoiles;
    Armee armee;

    public General(String nom, String grade, int sante, int force, int endurance, int competence, int nbEtoiles) {
        super(nom, grade, sante, force, endurance, competence);
        this.nbEtoiles = nbEtoiles;
    }

    public General(String nom, String grade, int sante, int force, int endurance, int competence, int nbEtoiles, Armee armee) {
        super(nom, grade, sante, force, endurance, competence);
        this.nbEtoiles = nbEtoiles;
        this.changeArmee(armee);
    }

    public int getNbEtoiles() {
        return nbEtoiles;
    }

    public void setNbEtoiles(int nbEtoiles) {
        this.nbEtoiles = nbEtoiles;
    }

    public Armee getArmee() {
        return armee;
    }

    public void setArmee(Armee armee) {
        this.armee = armee;
    }

    public void changeArmee(Armee armee)
    {
        if (armee != null) if (armee.getGeneral() != null) armee.getGeneral().setArmee(null);
        this.armee = armee;
        armee.setGeneral(this);
    }

    @Override
    public String toString() {
        return "General{" +
                "nbEtoiles=" + nbEtoiles +
                '}';
    }

    public void promeutSoldat(Soldat soldat)
    {
        // todo
    }
}
